var sessionUtils = require('./../services/sessionUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var redisUtils = require('./../services/redisUtils');
var util = require('util');

module.exports = {

    setCookie: function* (next) {
        this.cookies.set("editorCookie", "true", { httpOnly: false });
        this.body = 'OK';
    },

    unsetCookie: function* (next) {
        this.cookies.set("editorCookie", null, { httpOnly: false });
        this.body = 'OK';
    },

    showLoginPage: function* (next) {
        var errorMessage;
        yield this.render('login', {
            errorMessage: errorMessage
        });
    },

    showSignupPage: function* (next) {
        var errorMessage;
        yield this.render('signup', {
            errorMessage: errorMessage
        });
    },

    showHomePage: function* (next){
        var categoryList = yield redisUtils.getItem(Constants.redisDataKeys.CATEGORY_LIST);
        yield this.render('home', {
            categoryList: categoryList
        });
    },


    showTestPage: function* (next) {
        var categoryList = yield redisUtils.getItem(Constants.redisDataKeys.CATEGORY_LIST);
        yield this.render('test', {
            categoryList: categoryList
        });
    },

    showStagingAppPage: function* (next) {
        var categoryList = yield redisUtils.getItem(Constants.redisDataKeys.CATEGORY_LIST);
        yield this.render('teststaging', {
            categoryList: categoryList
        });
    },

    login: function* (next) {
        var email = this.request.body.email;
        var password = this.request.body.password;

        var queryString = "select * from user where email = '%s' and password = '%s'";
        var query = util.format(queryString, email, password);
        var results = yield databaseUtils.executeQuery(query);

        var errorMessage;
        if(results.length == 0) {
            errorMessage = "Incorrect user credentials";
            yield this.render('login', {
                errorMessage: errorMessage
            });
        } else {
            var redirectUrl = "/app/home";
            sessionUtils.saveUserInSession(results[0], this.cookies);
            this.redirect(redirectUrl);
        }
    },

    signup: function* (next) {

    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        this.redirect('/');
    }

   /* showTestPage: function* (next) {
        yield this.render('vwotest');
    }*/
}
